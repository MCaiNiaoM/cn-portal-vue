module.exports = {
  // 继承的规则
  extends: ['@commitlint/config-conventional'],
  // 定义规则类型
  rules: {
    // type 类型定义，表示git提交的type必须在以下类型范围内
    'type-enum': [
      2,
      'always',
      [
        'art', // 结构改进 / 格式化代码
        'zap', // 性能改善
        'fire', // 删除代码或者文件
        'bug', // 修了一个 BUG
        'ambulance', // 重大热修复
        'sparkles', // 引入新的特性
        'pencil', // 写文档
        'rocket', // 部署相关
        'lipstick', // 更新界面与样式文件
        'tada', // 创世提交
        'white_check_mark', // 更新测试
        'lock', // 修复安全问题
        'apple', // 修复在苹果系统上的问题
        'penguin', // 修复在 Linux 系统上的问题
        'checkered_flag', // 修复在 Windows 系统上的问题
        'robot', // 修复在安卓系统上的问题
        'green_apple', // 修复在 iOS 系统上的问题
        'bookmark', // 发布 / 版本标
        'rotating_light', // 消除 linter 警告
        'construction', // 进行中
        'green_heart', // 修复持续集成构建
        'arrow_down', // 降级依赖
        'arrow_up', // 升级依赖
        'pushpin', // 固定依赖在特定的版本
        'construction_worker', // 添加持续集成构建系统
        'chart_with_upwards_trend', // 添加分析或者跟踪代码
        'recycle', // 代码重构
        'whale', // Docker 容器相关
        'heavy_plus_sign', // 添加依赖
        'heavy_minus_sign', // 删除依赖
        'wrench', // 改变配置文件
        'globe_with_meridians', // 国际化与本地化
        'pencil2', // 修正拼写错误
        'poop', // 写需要改进的坏代码
        'rewind', // 回滚改动
        'twisted_rightwards_arrows', // 合并分支
        'package', // 更新编译后的文件或者包
        'alien', // 由于外部 API 变动而更新代码
        'truck', // 文件移动或者重命名
        'page_facing_up', // 添加或者更新许可
        'boom', // 引入破坏性的改动
        'bento', // 添加或者更新静态资源
        'ok_hand', // 代码审核后更新代码
        'wheelchair', // 改进可访问性
        'bulb', // 给源代码加文档
        'beers', // 醉写代码
        'speech_balloon', // 更新文本和字面
        'card_file_box', // 执行数据库相关的改动
        'loud_sound', // 添加日志
        'mute', // 删除日志
        'busts_in_silhouette', // 添加贡献者（们）
        'children_crossing', // 改进用户体验 / 可用性
        'building_construction', // 架构改动
        'iphone', // 响应性设计相关
        'clown_face', // 模拟相关
        'egg', // 添加一个彩蛋
        'see_no_evil', // 添加或者更新 .gitignore 文件
        'camera_flash', // 添加或者更新快照
        'alembic', // 研究新事物
        'mag', // 改进搜索引擎优化
        'wheel_of_dharma', // Kubernetes 相关的工作
        'label', // 添加或者更新类型（Flow, TypeScript）
      ],
    ],
    // subject 大小写不做校验
    'subject-case': [0],
  },
  prompt: {
    messages: {
      type: '选择你要提交的类型 :',
      scope: '选择一个提交范围（可选）:',
      customScope: '请输入自定义的提交范围 :',
      subject: '填写简短精炼的变更描述 :\n',
      body: '填写更加详细的变更描述（可选）。使用 "|" 换行 :\n',
      breaking: '列举非兼容性重大的变更（可选）。使用 "|" 换行 :\n',
      footerPrefixesSelect: '选择关联issue前缀(可选):',
      customFooterPrefix: '输入自定义issue前缀 :',
      footer: '列举关联issue (可选) 例如: #31, #I3244 :\n',
      generatingByAI: '正在通过 AI 生成你的提交简短描述...',
      generatedSelectByAI: '选择一个 AI 生成的简短描述:',
      confirmCommit: '是否提交或修改commit ?',
    },
    // prettier-ignore
    types: [
      {
        value: "sparkles",
        name: "✨ 引入新的特性",
        emoji: ":sparkles:"
      },
      {
        value: "bug",
        name: "🐛 修了一个 BUG",
        emoji: ":bug:"
      },
      {
        value: "lipstick",
        name: "💄 更新界面与样式文件",
        emoji: ":lipstick:"
      },
      {
        value: "construction",
        name: "🚧 进行中",
        emoji: ":construction:"
      },
      {
        value: "tada",
        name: "🎉 创世提交",
        emoji: ":tada:"
      },
      {
        value: "art",
        name: "🎨 结构改进 / 格式化代码",
        emoji: ":art:"
      },
      {
        value: "zap",
        name: "⚡️ 性能改善",
        emoji: ":zap:"
      },
      {
        value: "label",
        name: "🏷️ 添加或者更新类型(Flow, TypeScript)",
        emoji: ":label:"
      },
      {
        value: "pencil",
        name: "📝 写文档",
        emoji: ":pencil:"
      },
      {
        value: "fire",
        name: "🔥 删除代码或者文件",
        emoji: ":fire:"
      },
      {
        value: "rotating_light",
        name: "🚨 消除 linter 警告",
        emoji: ":rotating_light:"
      },
      {
        value: "wrench",
        name: "🔧 改变配置文件",
        emoji: ":wrench:"
      },
      {
        value: "globe_with_meridians",
        name: "🌐 国际化与本地化",
        emoji: ":globe_with_meridians:"
      },
      {
        value: "pencil2",
        name: "✏️ 修正拼写错误",
        emoji: ":pencil2:"
      },
      {
        value: "poop",
        name: "💩 写需要改进的坏代码",
        emoji: ":poop:"
      },
      {
        value: "rewind",
        name: "⏪ 回滚改动",
        emoji: ":rewind:"
      },
      {
        value: "twisted_rightwards_arrows",
        name: "🔀 合并分支",
        emoji: ":twisted_rightwards_arrows:"
      },
      {
        value: "heavy_plus_sign",
        name: "➕ 添加依赖",
        emoji: ":heavy_plus_sign:"
      },
      {
        value: "heavy_minus_sign",
        name: "➖ 删除依赖",
        emoji: ":heavy_minus_sign:"
      },
      {
        value: "arrow_down",
        name: "⬇️ 降级依赖",
        emoji: ":arrow_down:"
      },
      {
        value: "arrow_up",
        name: "⬆️ 升级依赖",
        emoji: ":arrow_up:"
      },
      {
        value: "pushpin",
        name: "📌 固定依赖在特定的版本",
        emoji: ":pushpin:"
      },
      {
        value: "bookmark",
        name: "🔖 发布 / 版本标",
        emoji: ":bookmark:"
      },
      {
        value: "ambulance",
        name: "🚑 重大热修复",
        emoji: ":ambulance:"
      },
      {
        value: "rocket",
        name: "🚀 部署相关",
        emoji: ":rocket:"
      },
      {
        value: "white_check_mark",
        name: "✅ 更新测试",
        emoji: ":white_check_mark:"
      },
      {
        value: "lock",
        name: "🔒 修复安全问题",
        emoji: ":lock:"
      },
      {
        value: "apple",
        name: "🍎 修复在苹果系统上的问题",
        emoji: ":apple:"
      },
      {
        value: "penguin",
        name: "🐧 修复在 Linux 系统上的问题",
        emoji: ":penguin:"
      },
      {
        value: "checkered_flag",
        name: "🏁 修复在 Windows 系统上的问题",
        emoji: ":checkered_flag:"
      },
      {
        value: "robot",
        name: "🤖 修复在安卓系统上的问题",
        emoji: ":robot:"
      },
      {
        value: "green_apple",
        name: "🍏 修复在 iOS 系统上的问题",
        emoji: ":green_apple:"
      },
      {
        value: "green_heart",
        name: "💚 修复持续集成构建",
        emoji: ":green_heart:"
      },
      {
        value: "construction_worker",
        name: "👷 添加持续集成构建系统",
        emoji: ":construction_worker:"
      },
      {
        value: "chart_with_upwards_trend",
        name: "📈 添加分析或者跟踪代码",
        emoji: ":chart_with_upwards_trend:"
      },
      {
        value: "recycle",
        name: "♻️ 代码重构",
        emoji: ":recycle:"
      },
      {
        value: "whale",
        name: "🐳 Docker 容器相关",
        emoji: ":whale:"
      },
      {
        value: "package",
        name: "📦 更新编译后的文件或者包",
        emoji: ":package:"
      },
      {
        value: "alien",
        name: "👽 由于外部 API 变动而更新代码",
        emoji: ":alien:"
      },
      {
        value: "truck",
        name: "🚚 文件移动或者重命名",
        emoji: ":truck:"
      },
      {
        value: "page_facing_up",
        name: "📄 添加或者更新许可",
        emoji: ":page_facing_up:"
      },
      {
        value: "boom",
        name: "💥 引入破坏性的改动",
        emoji: ":boom:"
      },
      {
        value: "bento",
        name: "🍱 添加或者更新静态资源",
        emoji: ":bento:"
      },
      {
        value: "ok_hand",
        name: "👌 代码审核后更新代码",
        emoji: ":ok_hand:"
      },
      {
        value: "wheelchair",
        name: "♿️ 改进可访问性",
        emoji: ":wheelchair:"
      },
      {
        value: "bulb",
        name: "💡 给源代码加文档",
        emoji: ":bulb:"
      },
      {
        value: "beers",
        name: "🍻 醉写代码",
        emoji: ":beers:"
      },
      {
        value: "speech_balloon",
        name: "💬 更新文本和字面",
        emoji: ":speech_balloon:"
      },
      {
        value: "card_file_box",
        name: "🗃 执行数据库相关的改动",
        emoji: ":card_file_box:"
      },
      {
        value: "loud_sound",
        name: "🔊 添加日志",
        emoji: ":loud_sound:"
      },
      {
        value: "mute",
        name: "🔇 删除日志",
        emoji: ":mute:"
      },
      {
        value: "busts_in_silhouette",
        name: "👥 添加贡献者（们）",
        emoji: ":busts_in_silhouette:"
      },
      {
        value: "children_crossing",
        name: "🚸 改进用户体验 / 可用性",
        emoji: ":children_crossing:"
      },
      {
        value: "building_construction",
        name: "🏗 架构改动",
        emoji: ":building_construction:"
      },
      {
        value: "iphone",
        name: "📱 响应性设计相关",
        emoji: ":iphone:"
      },
      {
        value: "clown_face",
        name: "🤡 模拟相关",
        emoji: ":clown_face:"
      },
      {
        value: "egg",
        name: "🥚 添加一个彩蛋",
        emoji: ":egg:"
      },
      {
        value: "see_no_evil",
        name: "🙈 添加或者更新 .gitignore 文件",
        emoji: ":see_no_evil:"
      },
      {
        value: "camera_flash",
        name: "📸 添加或者更新快照",
        emoji: ":camera_flash:"
      },
      {
        value: "alembic",
        name: "⚗ 研究新事物",
        emoji: ":alembic:"
      },
      {
        value: "mag",
        name: "🔍 改进搜索引擎优化",
        emoji: ":mag:"
      },
      {
        value: "wheel_of_dharma",
        name: "☸️ Kubernetes 相关的工作",
        emoji: ":wheel_of_dharma:"
      },
    ],
    useEmoji: true,
    emojiAlign: 'center',
    useAI: false,
    aiNumber: 1,
    themeColorCode: '',
    scopes: [],
    allowCustomScopes: true,
    allowEmptyScopes: true,
    customScopesAlign: 'bottom',
    customScopesAlias: 'custom',
    emptyScopesAlias: 'empty',
    upperCaseSubject: false,
    markBreakingChangeMode: false,
    allowBreakingChanges: ['feat', 'fix'],
    breaklineNumber: 100,
    breaklineChar: '|',
    skipQuestions: [],
    issuePrefixes: [{ value: 'closed', name: 'closed:   ISSUES has been processed' }],
    customIssuePrefixAlign: 'top',
    emptyIssuePrefixAlias: 'skip',
    customIssuePrefixAlias: 'custom',
    allowCustomIssuePrefix: true,
    allowEmptyIssuePrefix: true,
    confirmColorize: true,
    maxHeaderLength: Infinity,
    maxSubjectLength: Infinity,
    minSubjectLength: 0,
    scopeOverrides: undefined,
    defaultBody: '',
    defaultIssues: '',
    defaultScope: '',
    defaultSubject: '',
  },
};
