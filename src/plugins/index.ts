import type { App } from 'vue';
import router from '@/router';
import i18n from '@/locales';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import duration from 'dayjs/plugin/duration';

dayjs.extend(relativeTime);
dayjs.extend(duration);

import piniaPlugin from './pinia';
import {
  AutoComplete,
  Card,
  Cascader,
  Checkbox,
  Input,
  InputNumber,
  Mentions,
  Radio,
  Rate,
  Select,
  Switch,
  Transfer,
  TreeSelect,
  Slider,
  DatePicker,
  TimePicker,
  Alert,
  Divider,
  Space,
  Typography,
  notification,
} from 'ant-design-vue';
export default {
  install: (app: App) => {
    app
      .use(piniaPlugin)
      .use(i18n)
      .use(router)
      .use(Card)
      .use(Input)
      .use(AutoComplete)
      .use(InputNumber)
      .use(Switch)
      .use(Mentions)
      .use(Radio)
      .use(Checkbox)
      .use(Select)
      .use(Cascader)
      .use(Transfer)
      .use(TreeSelect)
      .use(Rate)
      .use(Slider)
      .use(TimePicker)
      .use(DatePicker)
      .use(Alert)
      .use(Divider)
      .use(Space)
      .use(Typography);
  },
};

notification.config({
  placement: 'bottomRight',
  maxCount: 1,
  duration: 3,
});
