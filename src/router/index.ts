import { createRouter, createWebHashHistory } from 'vue-router';

export default createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'IndexPage',
      meta: { title: 'menus.index' },
      component: () => import('@/views/home/home-page.vue'),
    },
  ],
});
