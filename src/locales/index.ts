import { createI18n } from 'vue-i18n';
import { config } from '@/settings/application';

import zh from './zh/';
import en from './en';

import dayjs from 'dayjs';

import en_US_ant from 'ant-design-vue/es/locale/en_US';
import zh_CN_ant from 'ant-design-vue/es/locale/zh_CN';

import day_zh from 'dayjs/locale/zh-cn';
import day_en from 'dayjs/locale/en';

export const messages = {
  zh_CN: {
    ...zh,
    ...zh_CN_ant,
  },
  en_US: {
    ...en,
    ...en_US_ant,
  },
};

const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: config.language,
  messages,
});

export function setI18nLanguage(lang: 'zh_CN' | 'en_US'): 'zh_CN' | 'en_US' {
  const html = document.querySelector('html');
  html?.setAttribute('lang', lang);
  dayjs.locale(lang === 'zh_CN' ? day_zh : day_en);
  return lang;
}
/** 翻译 */
export function translate(key: string): string {
  return i18n.global.t(key);
}

export default i18n;
