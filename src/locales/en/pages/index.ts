import login from './login.json';
import analysis from './analysis.json';

export default { login, analysis };
