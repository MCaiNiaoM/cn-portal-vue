import layout from './layout.json';
import menus from './menus.json';
import page from './pages';

export default { layout, menus, page };
