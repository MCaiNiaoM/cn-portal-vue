import layout from './layout.json';
import menus from './menus.json';
import notification from './notification.json';
import page from './pages';

export default { layout, menus, notification, page };
