import { config, type GlobalConfig } from '@/settings/application';
import { defineStore } from 'pinia';

export const useAppStore = defineStore('app', {
  state: (): GlobalConfig => {
    return { ...config };
  },
  getters: {
    isCN(state) {
      return state.language === 'zh_CN';
    },
  },
  actions: {
    updateConf(conf: unknown) {
      Object.assign(this, { layout: conf });
    },
    changePrimaryColor(color: string) {
      this.theme.primaryColor = color;
    },
    changeLocale(lang: 'zh_CN' | 'en_US') {
      this.language = lang;
    },
  },
  persist: {
    key: 'app',
    storage: localStorage,
  },
});
