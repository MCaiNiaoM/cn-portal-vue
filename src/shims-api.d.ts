/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Api } from './api';

declare global {
  interface Window {
    api: Api;
    WwLogin: any;
  }
}

declare module 'vue' {
  interface ComponentCustomProperties {
    $api: Api;
  }
}
