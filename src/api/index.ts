import type { App } from 'vue';
import oidcApi from './oidc/mods';

export const api = {
  oidcApi,
  install: (app: App) => {
    app.config.globalProperties.$api = api;
  },
};
export default api;
