declare namespace oidc {
  /**
   * AuthUser
   */
  export interface AuthUser {
    /** anonymous */
    anonymous?: boolean;

    /** 用户头像 */
    avatar?: string;

    /** 邮箱 */
    email?: string;

    /** extInfo */
    extInfo?: oidc.NutMap;

    /** 姓名 */
    fullName: string;

    /** 电话 */
    mobile?: string;

    /** 密码 */
    password: string;

    /** 权限列表 */
    permissions: Array<string>;

    /** jwt refreshToken */
    refreshToken: string;

    /** 角色列表 */
    roles: Array<string>;

    /** 性别 */
    sex?: 'MALE' | 'FEMALE';

    /** sexInfo */
    sexInfo?: oidc.Codebook;

    /** jwt Token */
    token: string;

    /** 用户名 */
    userName: string;
  }
}
