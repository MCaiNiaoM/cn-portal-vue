/**
 * @desc 登录
 */
import { defaultSuccess, defaultError, http } from '@/plugins/axios';
import type { AxiosResponse } from 'axios';
export interface Params {
  /** code */
  code?: string;
}

export default async function (
  params: Params,
  success: (data: oidc.AuthUser) => void = defaultSuccess,
  fail: (error: Error) => void = defaultError,
): Promise<void> {
  return http({
    method: 'post',
    url: `/oauth2/oidc/login`,

    params,
  })
    .then((data: AxiosResponse<oidc.AuthUser, unknown>) => {
      success(data.data);
    })
    .catch((error: Error) => fail(error));
}
