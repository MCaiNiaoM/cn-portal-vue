/**
 * @desc 获取登录地址
 */
import { defaultSuccess, defaultError, http } from '@/plugins/axios';
import type { AxiosResponse } from 'axios';
export interface Params {
  /** state */
  state?: string;
  /** nonce */
  nonce?: string;
}

export default async function (
  params: Params,
  success: (data: string) => void = defaultSuccess,
  fail: (error: Error) => void = defaultError,
): Promise<void> {
  return http({
    method: 'get',
    url: `/oauth2/oidc/login-url`,

    params,
  })
    .then((data: AxiosResponse<string, unknown>) => {
      success(data.data);
    })
    .catch((error: Error) => fail(error));
}
