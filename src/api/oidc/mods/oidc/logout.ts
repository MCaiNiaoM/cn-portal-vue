/**
 * @desc 退出登录
 */
import { defaultSuccess, defaultError, http } from '@/plugins/axios';
import type { AxiosResponse } from 'axios';
export default async function (
  success: (data: void) => void = defaultSuccess,
  fail: (error: Error) => void = defaultError,
): Promise<void> {
  return http({
    method: 'get',
    url: `/oauth2/oidc/logout`,
  })
    .then((data: AxiosResponse<void, unknown>) => {
      success(data.data);
    })
    .catch((error: Error) => fail(error));
}
