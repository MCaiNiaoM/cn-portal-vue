/**
 * @desc 获取登出地址
 */
import { defaultSuccess, defaultError, http } from '@/plugins/axios';
import type { AxiosResponse } from 'axios';
export default async function (
  success: (data: string) => void = defaultSuccess,
  fail: (error: Error) => void = defaultError,
): Promise<void> {
  return http({
    method: 'get',
    url: `/oauth2/oidc/logout-url`,
  })
    .then((data: AxiosResponse<string, unknown>) => {
      success(data.data);
    })
    .catch((error: Error) => fail(error));
}
