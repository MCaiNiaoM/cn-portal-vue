/**
 * @desc 当前登录用户
 */
import { defaultSuccess, defaultError, http } from '@/plugins/axios';
import type { AxiosResponse } from 'axios';
export default async function (
  success: (data: oidc.AuthUser) => void = defaultSuccess,
  fail: (error: Error) => void = defaultError,
): Promise<void> {
  return http({
    method: 'get',
    url: `/oauth2/oidc/current-user`,
  })
    .then((data: AxiosResponse<oidc.AuthUser, unknown>) => {
      success(data.data);
    })
    .catch((error: Error) => fail(error));
}
