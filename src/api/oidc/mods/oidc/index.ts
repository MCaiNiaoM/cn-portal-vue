/**
 * @description Open ID Connect
 *
 */
import currentUser from './currentUser';
import login from './login';
import loginUrl from './loginUrl';
import logout from './logout';
import logoutUrl from './logoutUrl';

export default {
  currentUser,
  login,
  loginUrl,
  logout,
  logoutUrl,
};
