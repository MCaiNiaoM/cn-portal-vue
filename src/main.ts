import { createApp } from 'vue';
import App from './App.vue';
import 'ant-design-vue/dist/reset.css';
import './style.css';
import plugins from './plugins';

createApp(App).use(plugins).mount('#app');
