export type AnimationNameValueType = 'none' | 'slide-fade-in-up' | 'slide-fade-in-right' | 'zoom-fade-in' | 'fade-in';

export interface Layout {
  showDrawer: boolean;
  headerTheme: 'light' | 'dark';
  navTheme: 'light' | 'dark' | 'real-dark';
  layout: 'side' | 'top' | 'mix';
  contentWidth: 'Fixed' | 'Fluid';
  splitMenus: boolean;
  collapsed: boolean;
  fixSiderbar: boolean;
  fixedHeader: boolean;
  collapsedWidth: number;
  sideBarWidth: number;
  mainSideBarWidth: number;
  disableContentMargin: boolean;
  headerHeight: number;
  waterMark: string;
  menu?: boolean;
  multiTab: boolean;
  multiTabFixed: boolean;
  colorWeak: boolean;
  gray: boolean;
  animationName: AnimationNameValueType;
}

export interface Theme {
  menuColor: string; // primary color of ant design
  primaryColor: string;
  colorWeak: boolean;
}

export interface GlobalConfig {
  layout: Layout;
  theme: Theme;
  language: 'zh_CN' | 'en_US';
  enableI8n: boolean;
  title: string;
  logo?: string;
  name: string;
  description: string;
  copyright: string;
  site: string;
  iconfontUrl: string;
}

export const config: GlobalConfig = {
  layout: {
    collapsed: false,
    waterMark: '',
    showDrawer: true,
    collapsedWidth: 64,
    headerHeight: 48,
    sideBarWidth: 208,
    mainSideBarWidth: 100,
    contentWidth: 'Fluid',
    disableContentMargin: false,
    fixSiderbar: true,
    fixedHeader: false,
    headerTheme: 'dark',
    layout: 'side',
    navTheme: 'light',
    splitMenus: true,
    menu: true,
    multiTab: false,
    multiTabFixed: false,
    colorWeak: false,
    gray: false,
    animationName: 'none',
  },
  theme: {
    menuColor: '#015b8a',
    primaryColor: '#283890',
    colorWeak: false,
  },
  language: 'zh_CN',
  iconfontUrl: '//at.alicdn.com/t/c/font_4663414_1w54ysia73i.js',
  title: 'milin',
  name: '神秘森林',
  description: '学习与分享',
  copyright: 'Copyright © 2023 - 2024  milin',
  site: 'https://www.baidu.com',
  enableI8n: true,
};
