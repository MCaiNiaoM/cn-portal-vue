FROM g-ziod8129-docker.pkg.coding.net/mysterious-forest/docker/cn-base-node:18.18.2 AS build
WORKDIR /app
COPY package.json pnpm-lock.yaml ./
RUN pnpm i
COPY . .
RUN pnpm build

FROM g-ziod8129-docker.pkg.coding.net/mysterious-forest/docker/cn-edge-gateway-nginx:1.25.4
# 创建目录，如果不存在的话
RUN mkdir -p /app/cn-portal
# 清空目录中的文件
RUN rm -rf /app/cn-portal/*
COPY --from=build /app/dist /app/cn-portal